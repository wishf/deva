﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Deva.Export
{
    public static class ExportHelper
    {
        public static Bitmap Export(int width, int height, Vector3[,] colours)
        {
            Bitmap bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            var region = new Rectangle(0, 0, width, height);
            var bmpData = bmp.LockBits(region, System.Drawing.Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat);

            IntPtr ptr = bmpData.Scan0;

            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbVals = new byte[bytes];

            for(int y = 0; y < height; y++)
            {
                for(int x = 0; x < width; x++)
                {
                    rgbVals[(y * Math.Abs(bmpData.Stride)) + (3*x) + 0] = (byte)(colours[x, y].Z * byte.MaxValue);
                    rgbVals[(y * Math.Abs(bmpData.Stride)) + (3*x) + 1] = (byte)(colours[x, y].Y * byte.MaxValue);
                    rgbVals[(y * Math.Abs(bmpData.Stride)) + (3*x) + 2] = (byte)(colours[x, y].X * byte.MaxValue);
                }
            }

            Marshal.Copy(rgbVals, 0, ptr, bytes);

            bmp.UnlockBits(bmpData);

            return bmp;
        }
    }
}
