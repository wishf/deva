﻿using Deva.Render;
using Deva.Scene;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Deva
{
    class Program
    {
        static void Main(string[] args)
        {
            var width = 640;
            var height = 480;

            var ruby = new Material
            {
                Ambient = new Vector3(0.1745f, 0.01175f, 0.01175f),
                Diffuse = new Vector3(0.61424f, 0.04136f, 0.04136f),
                Specular = new Vector3(0.727811f, 0.626959f, 0.626959f),
                Shininess = 76.8f,
                Reflectance = 0.4f,
                RefractiveIndex = 1.76f,
                Transparency = 0.4f
            };

            var pearl = new Material
            {
                Ambient = new Vector3(0.25f, 0.20725f, 0.20725f),
                Diffuse = new Vector3(1f, 0.829f, 0.829f),
                Specular = new Vector3(0.296648f, 0.296648f, 0.296648f),
                Shininess = 11.264f,
                Reflectance = 0.2f,
                RefractiveIndex = 1.53f,
                Transparency = 0.3f
            };

            var silver = new Material
            {
                Ambient = new Vector3(0.23125f, 0.23125f, 0.23125f),
                Diffuse = new Vector3(0.2775f, 0.2775f, 0.2775f),
                Specular = new Vector3(0.773911f, 0.773911f, 0.773911f),
                Shininess = 89.6f,
                Reflectance = 0.8f
            };

            var gold = new Material
            {
                Ambient = new Vector3(0.24725f, 0.2245f, 0.0645f),
                Diffuse = new Vector3(0.34615f, 0.3143f, 0.0903f),
                Specular = new Vector3(0.797357f, 0.723991f, 0.208006f),
                Shininess = 83.2f,
                Reflectance = 0.8f
            };

            var ivory = Material.MakeOpaque(new Vector3(1f, 1f, 0.9411f), 0.3f, 50f);
            var ebony = Material.MakeOpaque(new Vector3(0.3333f, 0.3647f, 0.3137f), 0.3f, 50f);
            var pine = Material.MakeOpaque(new Vector3(0.0039f, 0.4745f, 0.4352f), 0.1f, 0f);

            //var sphere = PrimitiveFields.Sphere(1f);
            var plane = PrimitiveFields.Plane(new Vector4(Vector3.UnitY, 1), pearl);
            var xplane = PrimitiveFields.Plane(new Vector4(-Vector3.UnitX, 1), silver);
            var zplane = PrimitiveFields.Plane(new Vector4(Vector3.UnitZ, 1), gold);
            var sphere = PrimitiveFields.Sphere(10f, ruby);
            var sphere2 = PrimitiveFields.Sphere(1f, ruby).RepeatX(4f).RepeatZ(4f);
            var sphere3 = PrimitiveFields.Sphere(1f, gold).RepeatX(4f).RepeatZ(4f);
            var cubes = PrimitiveFields.Box(new Vector3(0.5f, 0.5f, 0.5f), ruby).RepeatX(1.5f).RepeatZ(1.5f).RepeatY(0.5f);

            var ivoryCubes1 = PrimitiveFields.Box(new Vector3(0.5f, 0.5f, 0.5f), ivory).RepeatX(2f).RepeatZ(2f);
            var ivoryCubes2 = ivoryCubes1.Translate(new Vector3(1f, 0f, 1f));
            var ivoryCubes = ivoryCubes1 + ivoryCubes2;

            var ebonyCubes1 = PrimitiveFields.Box(new Vector3(0.5f, 0.5f, 0.5f), ebony).RepeatX(2f).RepeatZ(2f).Translate(new Vector3(1f, 0f, 0f));
            var ebonyCubes2 = ebonyCubes1.Translate(new Vector3(1f, 0f, 1f));
            var ebonyCubes = ebonyCubes1 + ebonyCubes2;

            var checkerboard = ivoryCubes + ebonyCubes;

            var goldBlock = PrimitiveFields.Box(new Vector3(0.5f, 0.5f, 0.5f), pearl);
            var silverBlock = PrimitiveFields.Box(new Vector3(0.5f, 0.5f, 0.5f), ruby).Translate(new Vector3(1f, 1f, 0f));

            var shinyTower = (goldBlock + silverBlock).Translate(new Vector3(0f, 1f, 0f));
            shinyTower = shinyTower + shinyTower.Translate(new Vector3(0f, 2f, 0f)) + shinyTower.Translate(new Vector3(0f, 4f, 0f));
            shinyTower = shinyTower.GaussianTwistXZ(1f, 3f, 24f).Translate(new Vector3(-2f, 0f, 2f));

            var cylinder = PrimitiveFields.Cylinder(3f, 0.75f, silver);
            // c = height
            // a and b are in some sort of ratio
            // ratio of width at base to height (must be normalised)
            var cone = PrimitiveFields.Cone(0.9f, 0.45f, 3f, gold);
            var tree = cone.Blend(cone.Translate(new Vector3(0, 1.25f, 0))).Blend(cone.Translate(new Vector3(0, 2.5f, 0)));
            tree = tree.Translate(new Vector3(0, 3f, 0)).Blend(cylinder);
            tree = tree.Translate(new Vector3(-3f, 3f, 3f));

            var hollowedSphere = sphere - ~cubes;
            var camera = Camera.CreatePerspectiveFieldOfView(Vector3.UnitY, new Vector3(-10f, 10f, 10f), Vector3.Zero, 45f, (float)width / (float)height, 1f, 10000f);
            //var camera = Camera.CreateOrthographic(Vector3.UnitY, new Vector3(-10f, 10f, 10f), Vector3.Zero, 3, 2, -1f, 1f);
            var scene = new Scene.Scene(tree + checkerboard, camera);

            var light = new PointLight(new Vector3(-10f, 8f, 10f), new Vector3(1f, 0.8392f, 0.6666f));
            scene.AddLight(light);

            var blueLight = new PointLight(new Vector3(-10f, 10f, 8f), new Vector3(0.8313f, 0.9215f, 1f));
            scene.AddLight(blueLight);

            //scene.Render(width, height);
            Task.WaitAll(scene.RenderMultithreaded(width, height, 8));
        }
    }
}
