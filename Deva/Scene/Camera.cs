﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Deva.Scene
{
    public class Camera
    {
        public Vector3 Up { get; }
        public Vector3 Position { get; }
        public Vector3 LookAt { get; }

        private Matrix4x4 projection;
        private Matrix4x4 translateCamera;
        private Quaternion rotateCamera;

        private Camera(Vector3 up, Vector3 position, Vector3 look)
        {
            Up = up;
            Position = position;
            LookAt = look;
            projection = Matrix4x4.Identity;
            translateCamera = Matrix4x4.CreateTranslation(-position);

            Vector3 zaxis = Vector3.Normalize(position - look);
            Vector3 xaxis = Vector3.Normalize(Vector3.Cross(up, zaxis));
            Vector3 yaxis = Vector3.Cross(zaxis, xaxis);

            rotateCamera = Quaternion.CreateFromRotationMatrix(new Matrix4x4
            {
                M11 = xaxis.X, M12 = yaxis.X, M13 = zaxis.X, M14 = 0,
                M21 = xaxis.Y, M22 = yaxis.Y, M23 = zaxis.Y, M24 = 0,
                M31 = xaxis.Z, M32 = yaxis.Z, M33 = zaxis.Z, M34 = 0,
                M41 = 0,       M42 = 0,       M43 = 0,       M44 = 1,
            });
        }

        public static Camera CreatePerspective(Vector3 up, Vector3 position, Vector3 look, float width, float height, float nearPlane, float farPlane)
        {
            var persp = Matrix4x4.CreatePerspective(width, height, nearPlane, farPlane);
            var camera = new Camera(up, position, look);
            camera.projection = persp;
            return camera;
        }

        public static Camera CreatePerspectiveFieldOfView(Vector3 up, Vector3 position, Vector3 look, float fov, float aspect, float nearPlane, float farPlane)
        {
            var persp = Matrix4x4.CreatePerspectiveFieldOfView((float)((Math.PI/180)*fov), aspect, nearPlane, farPlane);
            var camera = new Camera(up, position, look);
            camera.projection = persp;
            return camera;
        }

        public static Camera CreateOrthographic(Vector3 up, Vector3 position, Vector3 look, float width, float height, float zNear, float zFar)
        {
            var ortho = Matrix4x4.CreateOrthographic(width, height, zNear, zFar);
            var camera = new Camera(up, position, look);
            camera.projection = ortho;
            return camera;
        }

        public Matrix4x4 GetView()
        {
            return (translateCamera * Matrix4x4.CreateFromQuaternion(rotateCamera));
        }

        public Matrix4x4 GetProjection()
        {
            return projection;
        }
    }
}
