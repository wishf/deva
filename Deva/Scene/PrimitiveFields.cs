﻿using Deva.Extensions;
using Deva.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Deva.Scene
{
    public static class PrimitiveFields
    {
        public static DistanceField Sphere(float radius, Material material)
        {
            DistanceField.DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                mat = material;
                return point.Length() - radius;
            };

            return new DistanceField(func);
        }

        public static DistanceField Box(Vector3 dimensions, Material material)
        {
            DistanceField.DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                mat = material;
                Vector3 d = Vector3.Abs(point) - dimensions;
                return (float)Math.Min(Math.Max(d.X, Math.Max(d.Y, d.Z)), 0.0) + Vector3.Max(d, Vector3.Zero).Length();
            };

            return new DistanceField(func);
        }

        public static DistanceField Plane(Vector4 normal, Material material)
        {
            DistanceField.DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                mat = material;
                return Vector3.Dot(point, new Vector3(normal.X, normal.Y, normal.Z)) + normal.W;
            };

            return new DistanceField(func);
        }

        public static DistanceField Cylinder(float height, float radius, Material material)
        {
            DistanceField.DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                mat = material;
                var v2 = new Vector2(point.X, point.Z);
                return Math.Max(v2.Length() - radius, Math.Abs(point.Y) - height);
            };

            return new DistanceField(func);
        }

        public static DistanceField Cone(float a, float b, float c, Material material)
        {
            DistanceField.DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                mat = material;
                // This vector is the distance from the Y axis as well as the elevation of the point
                var dist = new Vector2(new Vector2(point.X, point.Z).Length(), point.Y);
                var d1 = -point.Y - c;
                var d2 = Math.Max(Vector2.Dot(dist, new Vector2(a, b)), point.Y);
                return Vector2.Max(new Vector2(d1, d2), Vector2.Zero).Length() + Math.Min(Math.Max(d1, d2), 0f);
            };

            return new DistanceField(func);
        }
    }
}
