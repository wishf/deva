﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Deva.Render;
using System.Numerics;
using Deva.Extensions;

namespace Deva.Scene
{
    public class DistanceField
    {
        public delegate float DistanceFunction(Vector3 point, out Material mat);

        private DistanceFunction func;
        private Matrix4x4 transform;
        private float scale;

        private static float NORM_EPSILON = 0.001f;
        private static Vector3 X_EPSILON = Vector3.UnitX * NORM_EPSILON;
        private static Vector3 Y_EPSILON = Vector3.UnitY * NORM_EPSILON;
        private static Vector3 Z_EPSILON = Vector3.UnitZ * NORM_EPSILON;

        private DistanceField(DistanceFunction f, Matrix4x4 transform, float scale)
        {
            func = f;
            this.transform = transform;
            this.scale = scale;
        }

        public DistanceField(DistanceFunction func)
        {
            this.func = func;
            this.transform = Matrix4x4.Identity;
            this.scale = 1f;
        }

        public Material Evaluate(Vector3 point, out float distance)
        {
            Material mat;
            distance = func(Vector3.Transform(point, transform)/scale, out mat) * scale;
            return mat;
        }

        public float Evaluate(Vector3 point)
        {
            Material mat;
            return func(Vector3.Transform(point, transform) / scale, out mat) * scale;
        }

        public Vector3 Normal(Vector3 point)
        {
            return Vector3.Normalize(new Vector3(
                Evaluate(point + X_EPSILON) - Evaluate(point - X_EPSILON),
                Evaluate(point + Y_EPSILON) - Evaluate(point - Y_EPSILON),
                Evaluate(point + Z_EPSILON) - Evaluate(point - Z_EPSILON)
            ));
        }

        public static DistanceField Translate(Vector3 origin, DistanceField d)
        {
            return DistanceField.Transform(Matrix4x4.CreateTranslation(origin), d);
        }

        public DistanceField Translate(Vector3 origin)
        {
            return DistanceField.Translate(origin, this);
        }

        public static DistanceField Rotate(Quaternion quat, DistanceField d)
        {
            return DistanceField.Transform(Matrix4x4.CreateFromQuaternion(quat), d);
        }

        public DistanceField Rotate(Quaternion quat)
        {
            return DistanceField.Rotate(quat, this);
        }

        public static DistanceField Rotate(Vector3 axis, float angle, DistanceField d)
        {
            return DistanceField.Transform(Matrix4x4.CreateFromAxisAngle(axis, angle), d);
        }

        public DistanceField Rotate(Vector3 axis, float angle)
        {
            return DistanceField.Rotate(axis, angle, this);
        }

        public static DistanceField Rotate(float yaw, float pitch, float roll, DistanceField d)
        {
            return DistanceField.Transform(Matrix4x4.CreateFromYawPitchRoll(yaw, pitch, roll), d);
        }

        public DistanceField Rotate(float yaw, float pitch, float roll)
        {
            return DistanceField.Rotate(yaw, pitch, roll, this);
        }
        
        private static DistanceField Transform(Matrix4x4 mat, DistanceField d)
        {
            Matrix4x4 inverted;
            bool invertable = Matrix4x4.Invert(mat, out inverted);

            // TODO: Exception if not invertable

            return new DistanceField(d.func, inverted * d.transform, d.scale);
        }

        public static DistanceField Scale(float factor, DistanceField d)
        {
            return new DistanceField(d.func, d.transform, d.scale * factor);
        }

        public DistanceField Scale(float factor)
        {
            return DistanceField.Scale(factor, this);
        }

        public static DistanceField Complement(DistanceField d)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                float dist;
                mat = d.Evaluate(point, out dist);
                return -dist;
            };

            return new DistanceField(func);
        }

        public DistanceField Complement()
        {
            return DistanceField.Complement(this);
        }

        public static DistanceField Difference(DistanceField b, DistanceField a)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                float aD;
                var m = a.Evaluate(point, out aD);
                float bD;
                var n = b.Evaluate(point, out bD);
                float max = Math.Max(-aD, bD);

                if(max == bD)
                {
                    mat = n;
                }
                else
                {
                    mat = m;
                }

                return max;
            };

            return new DistanceField(func);
        }

        public static DistanceField Intersection(DistanceField a, DistanceField b)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                float aD;
                var m = a.Evaluate(point, out aD);
                float bD;
                var n = b.Evaluate(point, out bD);
                float max = Math.Max(aD, bD);

                if (max == bD)
                {
                    mat = n;
                }
                else
                {
                    mat = m;
                }

                return max;
            };

            return new DistanceField(func);
        }

        public static DistanceField Union(DistanceField a, DistanceField b)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                float aD;
                var m = a.Evaluate(point, out aD);
                float bD;
                var n = b.Evaluate(point, out bD);
                float min = Math.Min(aD, bD);

                if (min == bD)
                {
                    mat = n;
                }
                else
                {
                    mat = m;
                }

                return min;
            };

            return new DistanceField(func);
        }

        public static DistanceField Blend(DistanceField a, DistanceField b)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                float aD;
                var m = a.Evaluate(point, out aD);
                float bD;
                var n = b.Evaluate(point, out bD);
                float min = MathHelper.Smin(aD, bD);

                if (min == bD)
                {
                    mat = n;
                }
                else
                {
                    mat = m;
                }

                return min;
            };

            return new DistanceField(func);
        }

        public DistanceField Blend(DistanceField b)
        {
            return DistanceField.Blend(this, b);
        }

        public static DistanceField operator /(DistanceField a, DistanceField b)
        {
            return DistanceField.Intersection(a, b);
        }

        public static DistanceField operator +(DistanceField a, DistanceField b)
        {
            return DistanceField.Union(a, b);
        }

        public static DistanceField operator -(DistanceField a, DistanceField b)
        {
            return DistanceField.Difference(a, b);
        }

        public static DistanceField operator ~(DistanceField a)
        {
            return DistanceField.Complement(a);
        }

        public static DistanceField RepeatX(DistanceField a, float spacing)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                point.X = MathHelper.Mod(point.X, spacing) - (0.5f * spacing);
                float distance;
                mat = a.Evaluate(point, out distance);
                return distance;
            };

            return new DistanceField(func);
        }

        public DistanceField RepeatX(float spacing)
        {
            return DistanceField.RepeatX(this, spacing);
        }

        public static DistanceField RepeatY(DistanceField a, float spacing)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                point.Y = MathHelper.Mod(point.Y, spacing) - (0.5f * spacing);
                float distance;
                mat = a.Evaluate(point, out distance);
                return distance;
            };

            return new DistanceField(func);
        }

        public DistanceField RepeatY(float spacing)
        {
            return DistanceField.RepeatY(this, spacing);
        }

        public static DistanceField RepeatZ(DistanceField a, float spacing)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                point.Z = MathHelper.Mod(point.Z, spacing) - (0.5f * spacing);
                float distance;
                mat = a.Evaluate(point, out distance);
                return distance;
            };

            return new DistanceField(func);
        }

        public DistanceField RepeatZ(float spacing)
        {
            return DistanceField.RepeatZ(this, spacing);
        }

        public static DistanceField TwistXZ(DistanceField d, float twistiness)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                float c = (float)Math.Cos((twistiness * point.Y));
                float s = (float)Math.Sin(twistiness * point.Y);
                point.X = point.X * c - point.Z * s;
                point.Z = point.X * s + point.Z * c;
                float distance;
                mat = d.Evaluate(point, out distance);
                return distance;
            };

            return new DistanceField(func);
        }

        public DistanceField TwistXZ(float twistiness)
        {
            return DistanceField.TwistXZ(this, twistiness);
        }

        public static DistanceField GaussianTwistXZ(DistanceField d, float twistiness, float mean, float falloff)
        {
            DistanceFunction func = delegate (Vector3 point, out Material mat)
            {
                float localTwisty = (float)Math.Pow(Math.E, -Math.Pow((point.Y - mean), 2) / falloff);
                float c = (float)Math.Cos((twistiness * localTwisty));
                float s = (float)Math.Sin(twistiness * localTwisty);
                point.X = point.X * c - point.Z * s;
                point.Z = point.X * s + point.Z * c;
                float distance;
                mat = d.Evaluate(point, out distance);
                return distance;
            };

            return new DistanceField(func);
        }

        public DistanceField GaussianTwistXZ(float twistiness, float mean, float falloff)
        {
            return DistanceField.GaussianTwistXZ(this, twistiness, mean, falloff);
        }
    }
}
