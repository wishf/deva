﻿using Deva.Export;
using Deva.Extensions;
using Deva.Render;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Deva.Scene
{
    public class Scene
    {
        private DistanceField worldMap;
        private Camera camera;
        private List<PointLight> lights;

        public Scene(DistanceField field, Camera camera)
        {
            worldMap = field;
            this.camera = camera;
            lights = new List<PointLight>();
        }

        public void AddLight(PointLight light)
        {
            lights.Add(light);
        }

        // TODO: Very basic
        private Vector3 MarchRay(Ray r, float minTime, float maxTime, float thresholdDistance, float reflectanceThreshold, float transmittanceThreshold)
        {
            int iterations = 0;
            for(float t = minTime; t < maxTime;)
            {
                float estimate;
                var material = worldMap.Evaluate(r.Origin + (r.Direction * t), out estimate);
                if (estimate < thresholdDistance)
                {
                    var point = r.Origin + (r.Direction * (t + estimate));
                    var normal = worldMap.Normal(point);

                    var cameraDirection = Vector3.Normalize(camera.Position - point);

                    var ambientOcclusion = AmbientOcclusion(point, normal, 0.07f, 5);

                    var contribution = ambientOcclusion * material.Ambient;
                    foreach (var light in lights)
                    {

                        var lightDir = light.Position - point;
                        var lightLength = lightDir.Length();
                        Ray lightRay = new Ray
                        {
                            Origin = point + normal * 0.1f,
                            Direction = Vector3.Normalize(lightDir),
                            RecursionDepth = 0
                        };
                        var intensity = light.Emissive * SoftShadowRay(lightRay, minTime, lightLength, thresholdDistance, 8);

                        var reflectionDir = Vector3.Normalize(2 * normal * Vector3.Dot(normal, lightRay.Direction) - lightRay.Direction);
                        if (material.Reflectance != 0 && r.AccumulatedReflectance >= reflectanceThreshold)
                        {
                            Ray reflectedRay = new Ray
                            {
                                Origin = point + normal * 0.1f,
                                Direction = reflectionDir,
                                RecursionDepth = r.RecursionDepth + 1,
                                AccumulatedReflectance = r.AccumulatedReflectance * material.Reflectance,
                                AccumulatedTransmittance = r.AccumulatedTransmittance
                            };
                            var reflectedColour = material.Reflectance * MarchRay(reflectedRay, minTime, maxTime, thresholdDistance, reflectanceThreshold, transmittanceThreshold);
                            contribution += reflectedColour;
                        }

                        var diffuse = material.Diffuse * MathHelper.Clamp(Vector3.Dot(normal, lightRay.Direction), 0, 1);
                        var specular = material.Specular * (float)Math.Pow(MathHelper.Clamp(Vector3.Dot(cameraDirection, reflectionDir), 0, 1), material.Shininess);
                        contribution += intensity * (diffuse + specular);
                    }

                    if (material.Transparency != 0 && r.InitialRefractiveIndex != material.RefractiveIndex && r.AccumulatedTransmittance >= transmittanceThreshold)
                    {
                        var iorRatio = r.InitialRefractiveIndex / material.RefractiveIndex;
                        var w = -(Vector3.Dot(r.Direction, normal)) * iorRatio;
                        var k = Math.Sqrt(1 + (w - iorRatio) * (w + iorRatio));
                        var refractionDir = Vector3.Normalize(iorRatio * r.Direction + (float)(w - k) * normal);
                        Ray refractionRay = new Ray
                        {
                            Origin = point,
                            Direction = refractionDir,
                            RecursionDepth = r.RecursionDepth + 1,
                            AccumulatedReflectance = r.AccumulatedReflectance,
                            AccumulatedTransmittance = r.AccumulatedTransmittance * material.Transparency
                        };
                        var transmittedColour = material.Transparency * MarchRay(refractionRay, minTime, maxTime, thresholdDistance, reflectanceThreshold, transmittanceThreshold);
                        contribution += transmittedColour;
                    }

                    return Vector3.Clamp(contribution, Vector3.Zero, Vector3.One);
                }
                t += estimate;
                iterations++;
            }

            return new Vector3(0f);
        }

        private float AmbientOcclusion(Vector3 point, Vector3 normal, float d, int iter)
        {
            float ao = 1;
            for(int i = iter; i > 0; i--)
            {
                ao -= (i * d - Math.Abs(worldMap.Evaluate(point + normal * i * d))) / (float)Math.Pow(2, i);
            }
            return MathHelper.Clamp(ao, 0, 1);
        }

        private float SoftShadowRay(Ray r, float minTime, float maxTime, float thresholdDistance, float k)
        {
            float res = 1f;
            for (float t = minTime; t < maxTime;)
            {
                float estimate = worldMap.Evaluate(r.Origin + (r.Direction * t));
                if (estimate < thresholdDistance)
                {
                    return 0f;
                }
                res = Math.Min(res, k * estimate / t);
                t += estimate;
            }

            return res;
        }

        public async void Render(int width, int height)
        {
            var colours = await Render(0, 0, width, height, width, height);

            using (var bmp = ExportHelper.Export(width, height, colours))
            {
                bmp.Save("scene.png", System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        public async Task<Vector3[,]> Render(int xOffset, int yOffset, int xBound, int yBound, int width, int height)
        {
            Matrix4x4 invProj;
            bool inverted = Matrix4x4.Invert(camera.GetProjection(), out invProj);

            // TODO: Handle case where not inverted correctly

            Matrix4x4 invView;
            inverted = Matrix4x4.Invert(camera.GetView(), out invView);

            // TODO: Handle case where not inverted correctly

            var invViewProj = invProj * invView;

            Vector3[,] colours = new Vector3[xBound, yBound];

            List<Task> tasks = new List<Task>();

            for(int y = 0; y < yBound; y++)
            {
                for(int x = 0; x < xBound; x++)
                {
                    float pX = 2 * (((x+xOffset)+0.5f) / width) - 1;
                    float pY = -2 * (((y+yOffset)+0.5f) / height) + 1;

                    var worldNear = Vector4.Transform(new Vector4(pX, pY, -1, 1), invViewProj);
                    var worldFar = Vector4.Transform(new Vector4(pX, pY, 1, 1), invViewProj);

                    var worldNearPos = new Vector3(worldNear.X, worldNear.Y, worldNear.Z) / worldNear.W;
                    var worldFarPos = new Vector3(worldFar.X, worldFar.Y, worldFar.Z) / worldFar.W;

                    var ray = new Ray
                    {
                        Origin = worldNearPos,
                        Direction = Vector3.Normalize(worldFarPos - worldNearPos),
                        RecursionDepth = 0,
                        AccumulatedReflectance = 1,
                        AccumulatedTransmittance = 1,
                        InitialRefractiveIndex = 1f
                    };

                    var task = new Task(delegate (object state)
                    {
                        var xy = (Tuple<int, int, Ray>)state;
                        colours[xy.Item1, xy.Item2] = MarchRay(xy.Item3, 0f, 500f, 0.001f, 0.05f, 0.05f);
                    }, new Tuple<int, int, Ray>(x, y, ray));

                    tasks.Add(task);
                    task.Start();
                }
            }

            await Task.WhenAll(tasks);

            return colours;
        }

        public async Task RenderMultithreaded(int width, int height, int threads)
        {
            // Split screen space into uniform size bands
            int bandHeight = height / threads;
            int unequalRows = height - bandHeight * threads;

            List<Rectangle> bands = new List<Rectangle>(threads);
            List<Task<Vector3[,]>> tasks = new List<Task<Vector3[,]>>(threads);

            int bandY = 0;

            for(int i = 0; i < threads; i++)
            {
                int currentBandHeight = bandHeight;

                if(unequalRows > 0)
                {
                    currentBandHeight++;
                    unequalRows--;
                }

                bands.Add(new Rectangle(0, bandY, width, currentBandHeight));
                bandY += currentBandHeight;
                tasks.Add(this.Render(bands[i].X, bands[i].Y, width, bands[i].Height, width, height));
            }

            int complete = 0;

            using (Bitmap bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
            {
                while (complete != threads)
                {
                    var task = await Task.WhenAny(tasks);

                    int idx = tasks.IndexOf(task);
                    tasks.Remove(task);

                    var region = bands[idx];
                    bands.Remove(region);
                    var colours = task.Result;
                    var bmpData = bmp.LockBits(region, System.Drawing.Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat);

                    IntPtr ptr = bmpData.Scan0;

                    int bytes = Math.Abs(bmpData.Stride) * region.Height;
                    byte[] rgbVals = new byte[bytes];

                    for (int y = 0; y < region.Height; y++)
                    {
                        for (int x = 0; x < region.Width; x++)
                        {
                            rgbVals[(y * Math.Abs(bmpData.Stride)) + (3 * x) + 0] = (byte)(colours[x, y].Z * byte.MaxValue);
                            rgbVals[(y * Math.Abs(bmpData.Stride)) + (3 * x) + 1] = (byte)(colours[x, y].Y * byte.MaxValue);
                            rgbVals[(y * Math.Abs(bmpData.Stride)) + (3 * x) + 2] = (byte)(colours[x, y].X * byte.MaxValue);
                        }
                    }

                    Marshal.Copy(rgbVals, 0, ptr, bytes);

                    bmp.UnlockBits(bmpData);
                    complete++;
                }

                bmp.Save("scene.png", System.Drawing.Imaging.ImageFormat.Png);
            }
        }
    }
}
