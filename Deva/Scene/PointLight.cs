﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Deva.Scene
{
    public class PointLight
    {
        public Vector3 Position { get; }
        public Vector3 Emissive { get; }

        public PointLight(Vector3 position, Vector3 emissive)
        {
            Position = position;
            Emissive = emissive;
        }
    }
}
