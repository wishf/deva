﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Deva.Render
{
    public class Material
    {
        public Vector3 Diffuse { get; set; }
        public Vector3 Ambient { get; set; }
        public Vector3 Specular { get; set; }
        public Vector3 Emissive { get; set; }
        public float Shininess { get; set; }
        public float Reflectance { get; set; }
        public float RefractiveIndex { get; set; }
        public float Transparency { get; set; }

        public static Material MakeOpaque(Vector3 colour, float reflectance, float spec)
        {
            return new Material
            {
                Diffuse = 0.7f * colour,
                Ambient = 0.2f * colour,
                Specular = 0.1f * colour,
                Reflectance = reflectance,
                Shininess = spec
            };
        }
    }
}
