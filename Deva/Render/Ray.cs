﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Deva.Render
{
    public struct Ray
    {
        public Vector3 Origin;
        public Vector3 Direction;
        public int RecursionDepth;
        public float AccumulatedReflectance;
        public float AccumulatedTransmittance;
        public float InitialRefractiveIndex;

        public Vector3 At(float time)
        {
            return Origin + (Direction * time);
        }
    }
}
