﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Deva.Render
{
    public struct IntersectInfo
    {
        public Material Material;
        public Vector3 Normal;
        public Vector3 HitPoint;
        public float IntersectTime;
    }
}
