﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deva.Extensions
{
    public static class MathHelper
    {
        public static float Clamp(float value, float min, float max)
        {
            value = (value > max) ? max : value;
            return (value < min) ? min : value;
        }

        // Mimics GLSL mod
        public static float Mod(float x, float y)
        {
            return x - y * (float)Math.Floor(x / y);
        }

        // Smin from: http://iquilezles.org/www/articles/smin/smin.htm
        public static float Smin(float a, float b, float k)
        {
            float h = Clamp(0.5f + 0.5f * (b - a) / k, 0.0f, 1.0f);
            return Mix(b, a, h) - k * h * (1.0f - h);
        }

        // Polynomial smin (k=0.1)
        public static float Smin(float a, float b)
        {
            return Smin(a, b, 0.1f);
        }

        // Mimics GLSL mix
        public static float Mix(float x, float y, float a)
        {
            return x * (1 - a) + y * a;
        }
    }
}
